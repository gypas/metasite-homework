package metasite;

import lombok.Getter;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Getter
public class Result {

    private final Map<String, Long> ag;
    private final Map<String, Long> hn;
    private final Map<String, Long> ou;
    private final Map<String, Long> vz;

    public Result() {
        ag = new ConcurrentHashMap<>();
        hn = new ConcurrentHashMap<>();
        ou = new ConcurrentHashMap<>();
        vz = new ConcurrentHashMap<>();
    }

    public void addWord(String word) {

        String entry = word.toLowerCase();
        int firstCharAscii = (int) entry.charAt(0);

        if (firstCharAscii < 104) {
            synchronized (ag) {
                addValueToMap(ag, entry);
            }
        } else if (firstCharAscii < 111) {
            synchronized (hn) {
                addValueToMap(hn, entry);
            }
        } else if (firstCharAscii < 118) {
            synchronized (ou) {
                addValueToMap(ou, entry);
            }
        } else {
            synchronized (vz) {
                addValueToMap(vz, entry);
            }
        }
    }

    private void addValueToMap(Map<String, Long> map, String word) {
        if (map.containsKey(word)) {
            map.put(word, map.get(word) + 1);
        } else {
            map.put(word, 1L);
        }
    }
}
