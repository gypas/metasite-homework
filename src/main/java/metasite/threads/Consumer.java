package metasite.threads;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import metasite.Result;

import java.text.BreakIterator;
import java.util.concurrent.BlockingQueue;

@Slf4j
@AllArgsConstructor
public class Consumer implements Runnable {

    private BlockingQueue<String> queue;
    private Result result;

    public void run() {

        log.debug("Consumer started");
        while (true) {
            String line;
            try {
                line = queue.take();
                if (line.equals("\n")) {
                    queue.put("\n");
                    log.debug("Consumed poison pill, consumer shutting down");
                    break;
                }
                countWords(line);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void countWords(String text) {

        BreakIterator breakIterator = BreakIterator.getWordInstance();
        breakIterator.setText(text);

        int lastIndex = breakIterator.first();
        while (BreakIterator.DONE != lastIndex) {
            int firstIndex = lastIndex;
            lastIndex = breakIterator.next();
            if (lastIndex != BreakIterator.DONE && Character.isLetter(text.charAt(firstIndex))) {
                result.addWord(text.substring(firstIndex, lastIndex));
            }
        }
    }
}
