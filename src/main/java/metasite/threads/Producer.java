package metasite.threads;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.concurrent.BlockingQueue;

@Slf4j
@AllArgsConstructor
public class Producer implements Runnable {

    private Path fileToRead;
    private BlockingQueue<String> queue;

    @Override
    public void run() {

        try {
            log.debug("Producer started");
            BufferedReader reader = Files.newBufferedReader(fileToRead);
            String line;

            while ((line = reader.readLine()) != null) {
                queue.put(line);
            }
            log.debug("Producer shutting down");

        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

}