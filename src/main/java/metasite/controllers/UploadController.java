package metasite.controllers;

import javassist.NotFoundException;
import lombok.extern.slf4j.Slf4j;
import metasite.Result;
import metasite.services.WordCounterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Slf4j
@Controller
public class UploadController {

    @Autowired
    WordCounterService wordCounterService;

    @Value("${file.directories.base}")
    private String baseFolder;
    @Value("${file.directories.upload}")
    private String uploadFolder;

    @GetMapping("/")
    public String index() {
        return "upload";
    }

    @PostMapping("/upload")
    public String singleFileUpload(@RequestParam("files") MultipartFile[] files,
                                   RedirectAttributes redirectAttributes, HttpSession httpSession) {

        if (files.length == 1 && files[0].isEmpty()) {
            redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
            return "redirect:/errorPage";
        }

        try {
            for (MultipartFile file : files) {
                writeFileFromMultipartFile(file, baseFolder + httpSession.getId() + "/" + uploadFolder);
            }

            Result result = wordCounterService.countWords(httpSession.getId());
            redirectAttributes = addResult(redirectAttributes, result);

            httpSession.invalidate();
        } catch (IOException | NotFoundException e) {
            log.warn(e.getMessage());
            redirectAttributes.addFlashAttribute("message", e.getMessage());
            return "redirect:/errorPage";
        }

        return "redirect:/uploadResult";
    }

    @GetMapping("/uploadResult")
    public String uploadStatus() {
        return "uploadResult";
    }

    @GetMapping("/errorPage")
    public String errorPage() {
        return "errorPage";
    }

    private RedirectAttributes addResult(RedirectAttributes redirectAttributes, Result result) {

        redirectAttributes.addFlashAttribute("ag", result.getAg());
        redirectAttributes.addFlashAttribute("hn", result.getHn());
        redirectAttributes.addFlashAttribute("ou", result.getOu());
        redirectAttributes.addFlashAttribute("vz", result.getVz());

        return redirectAttributes;
    }

    private void writeFileFromMultipartFile(MultipartFile file, String folder) throws IOException {

        byte[] bytes = file.getBytes();
        Path path = Paths.get(folder + file.getOriginalFilename());
        Files.createDirectories(path.getParent());
        Files.write(path, bytes);
    }
}