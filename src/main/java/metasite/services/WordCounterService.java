package metasite.services;

import javassist.NotFoundException;
import lombok.extern.slf4j.Slf4j;
import metasite.Result;
import metasite.threads.Consumer;
import metasite.threads.Producer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;
import java.util.concurrent.*;

@Service
@Slf4j
public class WordCounterService {

    @Value("${counter.threads.producer}")
    private int producerThreadCount;
    @Value("${counter.threads.consumer}")
    private int consumerThreadCount;
    @Value("${counter.queue.size}")
    private int queueSize;
    @Value("${file.directories.base}")
    private String baseFolder;
    @Value("${file.directories.upload}")
    private String uploadFolder;
    @Value("${file.directories.output}")
    private String outputFolder;

    public Result countWords(String id) throws NotFoundException {

        try {
            BlockingQueue<String> queue = new LinkedBlockingQueue<>(queueSize);
            Result result = new Result();

            File folder = new File(baseFolder + id + "/" + uploadFolder);
            File[] files = folder.listFiles();
            if (files == null) {
                throw new NotFoundException("No files found");
            }
            Future[] futures = new Future[files.length];

            ExecutorService producerExec = Executors.newFixedThreadPool(producerThreadCount);
            ExecutorService consumerExec = Executors.newFixedThreadPool(consumerThreadCount);
            //Launch producers and consumers
            for (int i = 0; i < files.length; i++) {
                futures[i] = producerExec.submit(new Producer(files[i].toPath(), queue));
            }
            for (int i = 0; i < consumerThreadCount; i++) {
                consumerExec.execute(new Consumer(queue, result));
            }
            //Wait for producers to stop
            for (Future future : futures) {
                future.get();
            }
            //Poison pill
            queue.put("\n");

            shutdownExecutorService(producerExec);
            shutdownExecutorService(consumerExec);

            writeResultToFiles(result, baseFolder + id + "/" + outputFolder);

            return result;

        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            return null;
        }
    }

    private void shutdownExecutorService(ExecutorService service) throws InterruptedException {
        service.shutdown();
        if (!service.awaitTermination(10, TimeUnit.SECONDS)) {
            log.warn("Failed to shutdown executor service in 10 seconds, forcing shutdown");
            service.shutdownNow();
        }
    }

    private void writeResultToFiles(Result result, String path) {
        try {
            Files.createDirectories(Paths.get(path));
            writeMapToFile(result.getAg(), path + "A-G.txt");
            writeMapToFile(result.getHn(), path + "H-N.txt");
            writeMapToFile(result.getOu(), path + "O-U.txt");
            writeMapToFile(result.getVz(), path + "V-Z.txt");
        } catch (IOException e) {
            log.error("Failed to write result to files: " + e.getMessage());
        }
    }

    private void writeMapToFile(Map<String, Long> map, String fileName) throws IOException {

        BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));

        for (Map.Entry<String, Long> entry : map.entrySet()) {
            writer.write(entry.getKey() + ": " + entry.getValue() + "\n");
        }

        writer.close();
    }
}